plib.extensions
===============

The PLIB.EXTENSIONS package contains functions (and possibly,
in the future, other objects) exported from an extension
module written using the Python/C API. The general
philosophy of PLIB is to do everything possible in pure
Python, so the only functions that appear in this package
are those which by their very nature cannot be done in pure
Python.

Note: PLIB.EXTENSIONS works with Python 2.7. If you are using
Python 3, see the PLIB3.EXTENSIONS package, available at
https://gitlab.com/pdonis/plib3-extensions.

The ``setup.py`` script for PLIB.EXTENSIONS uses the ``setuputils``
helper module, which helps to automate away much of the
boilerplate in Python setup scripts. This module is available
as a separate release at https://gitlab.com/pdonis/setuputils.

The PLIB.EXTENSIONS Package
---------------------------

The following classes are available in the ``plib.extensions`` namespace:

- The ``capsule_compare`` function checks whether two Capsules
  wrap the same C-level pointer.

- The ``cobject_compare`` function checks whether two CObjects
  wrap the same C-level pointer.

Installation
------------

To install PLIB.EXTENSIONS, you can simply run::

    $ python setup.py install

at a shell prompt from the directory into which you
unzipped the source tarball (the same directory that this
README file is in). This will install PLIB and then
run each of the post-install scripts in the scripts
directory.

The Zen of PLIB
---------------

There is no single unifying purpose or theme to PLIB, but
like Python itself, it does have a 'Zen' of sorts:

- Express everything possible in terms of built-in Python
  data structures.

- Once you've expressed it that way, what the code is
  going to do with it should be obvious.

- Avoid boilerplate code, *and* boilerplate data. Every
  piece of data your program needs should have one and
  only one source.

Copyright and License
---------------------

PLIB.EXTENSIONS is Copyright (C) 2008-2019 by Peter A. Donis.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version. (See the LICENSE file for a
copy of version 2 of the License.)

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
